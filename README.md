# LinkedIn Profile Scrapper

Scrapping LinkedIn user data using selenium and BeautifulSoup

---

## Environment Setup

LinkedIn Profile Scrapper requirements are shown below

1. Python 3
2. WebDriver for Chrome
3. selenium package for Python
4. bs4 Python (Beautifulsoup)
5. requests package for Python

First install **Python 3** and **Python package manager**.  
Then install required packages, selenium and bs4, using command  
**`pip install selenium bs4 requests`** or **`pip3 install selenium bs4 requests`** 

---

## Installation

Install LinkedIn Scrapper using command **`git clone https://hanjiangyu@bitbucket.org/hanjiangyu/linkedin-profile-scrapper.git`**

---

## Parameter Setup

The user needs to enter certain information into **`parameters.py`** file proper scrap profiles

1. Locate path of the installed webdriver for Chrome, then specify the path in **`parameters.py`**  
   webdriver path can be found in bash via command  **`which webdriver`** 
2. Enter LinkedIn credentials into **`linkedin_username`** and **`linkedin_password`** respectively
3. Enter desired output file name in **`output_file_name`** field that ends with **`.csv`**
4. Enter search requirments that connect one another using **`AND`**  
   For example: To search a profile with name **Jeremy** and school **University of Maryland**, the search requirement field should be
   **`Jeremy AND University of Maryland`**
5. Enter desired number of entries shown on the search result page into the field **`num_page_result`**  
   Default value of **`num_page_result`** is **10**
6. Enter request catch url into field **`requests_url`**  
   Scrapper uses requests package to do curl post commands

---

## Run Program

After all previous steps are taken, please run the program by running shell command (in the repo directory): **`python3 linkedin_fetch.py`**  
Profile information will also be stored in **`linkedin_fetch.py`** as variable **`profile_results`**