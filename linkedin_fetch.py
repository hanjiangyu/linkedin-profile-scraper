from selenium import webdriver
from bs4 import BeautifulSoup
import re, time, csv, parameters, requests, sys

profile_results = []

# Parameters for script
if len(parameters.search_requirements) > 0:
    search_query = 'site:linkedin.com/in/ AND ' + parameters.search_requirements
else:
    search_query = 'site:linkedin.com/in/'

# Chrome Webdriver Import 
try:
    driver = webdriver.Chrome(parameters.webdriver_path)
except:
    print("No driver found")

with open(parameters.output_file_name, mode='w') as writeFile:
    linkedin_writer = csv.writer(writeFile)
    linkedin_writer.writerow(['First Name', 'Last Name', 'Current Occupation', 'Location', 'Job Titles', 'Companies','Colleges','Skills','Phone Number', 'Contacts', 'Birthday', 'LinkedIn Link'])
writeFile.close()

driver.get('https://www.linkedin.com/login')

try:
    # Send user name to Linkedin Website
    user_name = driver.find_element_by_name("session_key")
    user_name.send_keys(parameters.linkedin_username)

    # Send password to website
    password = driver.find_element_by_name("session_password")
    password.send_keys(parameters.linkedin_password)

    # Clicking sign in button
    sign_in = driver.find_element_by_xpath('//*[@type="submit"]')
    sign_in.click()
except:
    print("Could not log in LinkedIn")
    
time.sleep(2)

# Getting to the search website
driver.get('https://www.google.com/search?q=' + search_query+ '&num=' + parameters.num_page_result)

# Finding the next_page button on the first page
next_page = driver.find_element_by_xpath('//*[@id="pnnext"]/span[2]')

# Checking next_page
# While there are still pages left, check each linkedin profile.
while next_page != None:


    # Setting search_url as the current_url to go back after each while loop iteration
    search_url = str(driver.current_url)

    # Extract linkedin urls from search
    # linkedin_urls = driver.find_elements_by_class_name('iUh30')
    # linkedin_urls = [url.text for url in linkedin_urls]

    page = requests.get(driver.current_url)
    soup = BeautifulSoup(page.content, 'html.parser')
    links = soup.findAll("a")

    linkedin_urls = []
    for link in  soup.find_all("a",href=re.compile("(?<=/url\?q=)(htt.*://.*)")):
        linkedin_urls.append(re.split(":(?=http)",link["href"].replace("/url?q=","")))

    # Iterate each linkedin_url on the search page (10 urls/page)
    for [linkedin_url] in linkedin_urls:
        # Using regular expression to parse each link
        # url_group = re.search(r"(https://.*\.linkedin\.com).*\›\s*(.+)", linkedin_url)
        # if url_group:
        #     url = url_group.group(1) + '/in/' + url_group.group(2)
        #     driver.get(url)
        url_group= re.search(r"(.*\.linkedin\.com/in/.*)&sa=U.*", linkedin_url)
        try:
            current_url = url_group.group(1)
            driver.get(current_url)
        except:
            print("No such link", file=sys.stderr)
            pass
        time.sleep(5)
        
        try:
            # Start parsing page information using BeautifulSoup
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            
            # Array for multiple inputs
            job_title_input = []
            company_input = []
            college_input = []
            skills_input = []
            first_name = ''
            last_name = ''

            # Getting name of profile
            name = soup.find('li', attrs={'class':'inline t-24 t-black t-normal break-words'})
            if name:
                name = name.text.strip()
                name_group = re.search('(.*)\s(.*)', name)
                try:
                    first_name = name_group.group(1)
                    last_name = name_group.group(2)
                except:
                    print('Name Error')
                    pass
            else:
                name = ''
            
            # Getting current occupation
            current_occupation = soup.find('h2', attrs={'class':'mt1 t-18 t-black t-normal'})
            if current_occupation:
                current_occupation = current_occupation.text.strip()
            else:
                current_occupation = ''

            # Getting an array of job_titles
            job_titles = soup.find_all('h3', attrs={'class':'t-16 t-black t-bold'})
            for job_title in job_titles:
                if job_title:
                    job_title = job_title.text.strip()
                    job_title_input.append(job_title)
                else:
                    job_title = ''

            # Getting an array of companies
            companies = soup.find_all('p', attrs={'class':'pv-entity__secondary-title t-14 t-black t-normal'})
            for company in companies:
                if company:
                    company = company.text.strip()
                    company_input.append(company)
                else:
                    company = ''

            # Getting an array of colleges
            colleges = soup.find_all('h3', attrs={'class':'pv-entity__school-name t-16 t-black t-bold'})
            for college in colleges:  
                if college:
                    college = college.text.strip()
                    college_input.append(college)
                else:
                    college = ''
            
            # Finding skills shown in profile
            skills = soup.find_all('span', attrs={'class':'pv-skill-category-entity__name-text t-16 t-black t-bold'})
            for skill in skills: 
                if skill:
                    skill = skill.text.strip()
                    skills_input.append(skill)
                else:
                    skill = ''

            # Finding location in profile
            location = soup.find('li', attrs={'li':'t-16 t-black t-normal inline-block'})
            if location:
                location = location.text.strip()
            else:
                location = ''

            # Finding contact information in profile
            phone_number = soup.find('span', attrs={'class':'t-14 t-black t-normal'})
            if phone_number:
                phone_number = phone_number.text.strip()
            else:
                phone_number = ''

            contact_input = []
            contacts = soup.find_all('a', attrs={'class':'pv-contact-info__contact-link t-14 t-black t-normal'})
            for contact in contacts:
                if contact:
                    contact = contact.text.strip()
                    contact_input.append(contact)
                else:
                    contact = ''
            
            # Parsing birthday of the person shown in profile
            birthday = soup.find('span', attrs={'class':'pv-contact-info__contact-item t-14 t-black t-normal'})
            if birthday:
                birthday = birthday.text.strip()
            else:
                birthday = ''
            
            # Appending current profile values into variable profile_results
            current_profile_array = [first_name, last_name, current_occupation, location, job_title_input, 
                company_input, college_input, skills_input, phone_number, contact_input, birthday, current_url]

            current_profile = {'firstname': first_name, 
                'lastname': last_name,
                'current_occupation': current_occupation,
                'location': location,
                'job titles': job_title_input,
                'companies': company_input,
                'colleges': college_input,
                'skills': skills_input,
                'phone': phone_number,
                'contacts' : contact_input,
                'birthday': birthday,
                'linkedin url': current_url
                }
        except:
            print('Cannot parse current profile')
            pass

        profile_results.append(current_profile)
        try:
            r = requests.post(parameters.request_url, data=current_profile)
        except:
            print('Post Request Failed')

        # Write information into a csv file
        with open(parameters.output_file_name, mode='a') as writeFile:
            linkedin_writer = csv.writer(writeFile)
            linkedin_writer.writerow(current_profile_array)
        writeFile.close()

    # Back to the search page and extract information from the next page.
    try :
        driver.get(search_url)
        time.sleep(1)
        next_page = driver.find_element_by_xpath('//*[@id="pnnext"]/span[2]')
        next_page.click()
    except:
        print("There are no next page found exiting program", file=sys.stderr)
    time.sleep(1)

driver.close
