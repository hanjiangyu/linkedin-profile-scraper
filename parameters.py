"""
Parameters file is required to be set up by user.
"""

# Please indicate your webdriver path in the variable below.
# Default path would be the webdriver default path for macOS
webdriver_path = '/usr/local/bin/chromedriver'

# Please enter linkedin credentials below
linkedin_username = ''
linkedin_password = ''

# Please change the output_file_name to the desired name, default name would be data.csv
output_file_name = 'data.csv'

# Please enter desired search query in search_query below. 
# The format for search query should be (requirement 1) AND (requirement 2) if multiple requirements are used
# The scrapper will scrap all profiles if it is left empty
search_requirements = ''

# Number of results per search page
num_page_result = '10'

# Request Catch Url
request_url = ''